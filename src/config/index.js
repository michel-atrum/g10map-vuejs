export default {
  baseUrl: 'static/images/map/',
  areas: [
    {
      id: 'a0',
      name: 'ギラバニア辺境地帯',
      img: 'a0.jpg',
      locations: [
        {name: 'A', id: 'a0a', img: 'no_img.jpg', xy: [15.5, 16.5]},
        {name: 'B', id: 'a0b', img: 'no_img.jpg', xy: [9.3, 12.5]},
        {name: 'C', id: 'a0c', img: 'a0_c.jpg', xy: [11.1, 31.1]},
        {name: 'D', id: 'a0d', img: 'no_img.jpg', xy: [31.5, 20.8]},
        {name: 'E', id: 'a0e', img: 'no_img.jpg', xy: [8.3, 28.2]},
        {name: 'F', id: 'a0f', img: 'no_img.jpg', xy: [25.7, 20.1]},
        {name: 'G', id: 'a0g', img: 'a0_g.jpg', xy: [23.4, 12.2]},
        {name: 'H', id: 'a0h', img: 'no_img.jpg', xy: [33.0, 31.4]}
      ]
    },
    {
      id: 'a1',
      name: 'ギラバニア山岳地帯',
      img: 'a1.jpg',
      locations: [
        {name: 'A', id: 'a1a', img: 'no_img.jpg', xy: [8.6, 32.5]},
        {name: 'B', id: 'a1b', img: 'no_img.jpg', xy: [16.1, 9.2]},
        {name: 'C', id: 'a1c', img: 'no_img.jpg', xy: [24.0, 31.6]},
        {name: 'D', id: 'a1d', img: 'no_img.jpg', xy: [15.7, 33.1]},
        {name: 'E', id: 'a1e', img: 'no_img.jpg', xy: [15.4, 22.9]},
        {name: 'F', id: 'a1f', img: 'no_img.jpg', xy: [26.0, 13.7]},
        {name: 'G', id: 'a1g', img: 'no_img.jpg', xy: [12.2, 5.5]},
        {name: 'H', id: 'a1h', img: 'no_img.jpg', xy: [33.1, 9.8]}
      ]
    },
    {
      id: 'a2',
      name: 'ギラバニア湖畔地帯',
      img: 'a2.jpg',
      locations: [
        {name: 'A', id: 'a2a', img: 'no_img.jpg', xy: [8.6, 17.7]},
        {name: 'B', id: 'a2b', img: 'no_img.jpg', xy: [5.8, 6.7]},
        {name: 'C', id: 'a2c', img: 'no_img.jpg', xy: [31.9, 6.4]},
        {name: 'D', id: 'a2d', img: 'no_img.jpg', xy: [25.1, 7.8]},
        {name: 'E', id: 'a2e', img: 'no_img.jpg', xy: [19.6, 23.3]},
        {name: 'F', id: 'a2f', img: 'no_img.jpg', xy: [15.9, 28.6]},
        {name: 'G', id: 'a2g', img: 'no_img.jpg', xy: [24.5, 32.0]},
        {name: 'H', id: 'a2h', img: 'no_img.jpg', xy: [29.7, 17.4]}
      ]
    },
    {
      id: 'a3',
      name: '紅玉海',
      img: 'a3.jpg',
      locations: [
        {name: 'A', id: 'a3a', img: 'no_img.jpg', xy: [7.7, 29.7]},
        {name: 'B', id: 'a3b', img: 'no_img.jpg', xy: [5.2, 15.1]},
        {name: 'C', id: 'a3c', img: 'a3_c.jpg', xy: [32.9, 18.4]},
        {name: 'D', id: 'a3d', img: 'no_img.jpg', xy: [27.7, 30.0]},
        {name: 'E', id: 'a3e', img: 'no_img.jpg', xy: [30.5, 25.4]},
        {name: 'F', id: 'a3f', img: 'no_img.jpg', xy: [16.9, 9.6]},
        {name: 'G', id: 'a3g', img: 'no_img.jpg', xy: [18.4, 36.3]},
        {name: 'H', id: 'a3h', img: 'no_img.jpg', xy: [32.7, 8.8]}
      ]
    },
    {
      id: 'a4',
      name: 'ヤンサ',
      img: 'a4.jpg',
      locations: [
        {name: 'A', id: 'a4a', img: 'no_img.jpg', xy: [31.0, 28.5]},
        {name: 'B', id: 'a4b', img: 'no_img.jpg', xy: [13.2, 30.1]},
        {name: 'C', id: 'a4c', img: 'a4_c.jpg', xy: [31.8, 35.5]},
        {name: 'D', id: 'a4d', img: 'no_img.jpg', xy: [22.2, 27.8]},
        {name: 'E', id: 'a4e', img: 'no_img.jpg', xy: [32.0, 5.4]},
        {name: 'F', id: 'a4f', img: 'a4_f.jpg', xy: [20.0, 5.5]},
        {name: 'G', id: 'a4g', img: 'no_img.jpg', xy: [21.8, 13.9]},
        {name: 'H', id: 'a4h', img: 'no_img.jpg', xy: [12.0, 18.9]}
      ]
    },
    {
      id: 'a5',
      name: 'アジムステップ',
      img: 'a5.jpg?20180527',
      locations: [
        {name: 'A', id: 'a5a', img: 'no_img.jpg', xy: [4.9, 25.5]},
        {name: 'B', id: 'a5a', img: 'no_img.jpg', xy: [21.7, 36.9]},
        {name: 'C', id: 'a5a', img: 'no_img.jpg', xy: [30.9, 22.1]},
        {name: 'D', id: 'a5a', img: 'no_img.jpg', xy: [29.2, 35.3]},
        {name: 'E', id: 'a5a', img: 'no_img.jpg', xy: [29.5, 11.8]},
        {name: 'F', id: 'a5a', img: 'no_img.jpg', xy: [16.5, 25.0]},
        {name: 'G', id: 'a5a', img: 'no_img.jpg', xy: [14.9, 33.0]},
        {name: 'H', id: 'a5a', img: 'no_img.jpg', xy: [10.6, 15.9]}
      ]
    }
  ],
  aliases: [
    {id: 'a0', aliase: 'ギラバニア辺境地帯'},
    {id: 'a0', aliase: '辺境地帯'},
    {id: 'a0', aliase: '辺境'},
    {id: 'a1', aliase: 'ギラバニア山岳地帯'},
    {id: 'a1', aliase: '山岳地帯'},
    {id: 'a1', aliase: '山岳'},
    {id: 'a2', aliase: 'ギラバニア湖畔地帯'},
    {id: 'a2', aliase: '湖畔地帯'},
    {id: 'a2', aliase: '湖畔'},
    {id: 'a3', aliase: '紅玉海'},
    {id: 'a4', aliase: 'ヤンサ'},
    {id: 'a5', aliase: 'アジムステップ'},
    {id: 'a5', aliase: 'アジム'},
    {id: 'a5', aliase: '安心院'}
  ],
  aetherytes: [
    {idx: 1, name: 'カストルム・オリエンス', areaId: 'a0', xy: [8.8, 11.3], weight: 7},
    {idx: 2, name: 'ピーリングストーンズ', areaId: 'a0', xy: [29.7, 26.3], weight: 7},

    {idx: 11, name: 'アラガーナ', areaId: 'a1', xy: [23.8, 6.5], weight: 7},
    {idx: 12, name: 'アラギリ', areaId: 'a1', xy: [16.0, 36.4], weight: 7},

    {idx: 21, name: 'ポルタ・プレトリア', areaId: 'a2', xy: [8.3, 21.1], weight: 7},
    {idx: 22, name: 'アラミガン・クォーター', areaId: 'a2', xy: [33.7, 34.6], weight: 7},

    {idx: 31, name: 'オノコロ島', areaId: 'a3', xy: [23.2, 9.7], weight: 7},
    {idx: 32, name: '碧のタマミズ', areaId: 'a3', xy: [28.5, 16.1], weight: 20},
    {idx: 33, name: 'クガネより紅玉海方面', areaId: 'a3', xy: [38.3, 38.3], weight: 15},

    {idx: 41, name: 'ナマイ村', areaId: 'a4', xy: [30.1, 19.7], weight: 7},
    {idx: 42, name: '烈士庵', areaId: 'a4', xy: [26.4, 13.3], weight: 8},

    {idx: 51, name: '再会の市', areaId: 'a5', xy: [32.6, 28.2], weight: 7},
    {idx: 52, name: '明けの玉座', areaId: 'a5', xy: [23.0, 22.1], weight: 7},
    {idx: 53, name: 'ドーロ・イロー', areaId: 'a5', xy: [6.3, 23.8], weight: 7}
  ]
}

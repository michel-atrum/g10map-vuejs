import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Help from '@/components/Help'
import Info from '@/components/Info'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      redirect: {name: 'area', params: {areaId: 'a0'}}
    },
    {
      path: '/area/:areaId',
      name: 'area',
      component: Home,
      props: (route) => ({
        areaId: route.params.areaId,
        uuid: route.query.uuid
      })
    },
    {
      path: '/help',
      name: 'help',
      component: Help
    },
    {
      path: '/info',
      name: 'info',
      component: Info
    }
  ]
})
